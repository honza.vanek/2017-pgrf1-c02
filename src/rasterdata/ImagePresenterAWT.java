package rasterdata;

import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImagePresenterAWT<AnyPixelType>
        implements ImagePresenter<AnyPixelType, Graphics> {
    @NotNull
    @Override
    public Graphics present(@NotNull RasterImage<AnyPixelType> image,
                            @NotNull Graphics device) {
        if (image instanceof RasterImageAWT){
            BufferedImage img = ((RasterImageAWT) image).getImg();
            device.drawImage(img, 0, 0, null);
        } else {
            System.err.println("Cannot present image of type " + image.getClass());
        }
        return device;
    }
}
