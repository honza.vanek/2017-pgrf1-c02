import org.jetbrains.annotations.NotNull;
import rasterdata.*;
import rasterizationops.LineRasterizer;
import rasterizationops.LineRasterizerNaive;
import rasterizationops.SeedFill4;
import solidops.RenderWireframe;
import solids.Cube;
import transforms.Camera;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */

/* terminy uloh
uloha1: 26.12.2017
uloha2: 31.12.2017
uloha3: 7.1.2018
 */

public class Canvas {

	private final JFrame frame;
	private final JPanel panel;
	private final BufferedImage img;

	private @NotNull RasterImage<Color> rasterImage;
	private final @NotNull ImagePresenter<Color, Graphics> presenter;

	private final @NotNull LineRasterizer<Color> lineRasterizer;
	private int startC, startR;

	public Canvas(final int width, final int height) {
		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//*
		rasterImage = new RasterImageAWT<>(
			img,
			//Function<PixelType,Integer>, kde PixelType = Color
				(Color c) -> c.getRGB(),
			//Function<Integer,PixelType>, kde PixelType = Color
				(Integer i) -> new Color(i)
		);
		presenter = new ImagePresenterAWT<>();
/*/
		rasterImage = RasterImageImmu.cleared(
				width, height, new Color(255,0,0));
		presenter = new ImagePresenterUniversal<>(color -> color.getRGB());
//*/
		lineRasterizer = new LineRasterizerNaive<>();

		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				startC = e.getX();
				startR = e.getY();
			}
		});
		panel.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				final int endC = e.getX();
				final int endR = e.getY();
				final double x1 =
						2 * (startC + 0.5) / rasterImage.getWidth() - 1;
				final double y1 =
						-(2 * (startR + 0.5) / rasterImage.getHeight() - 1);
				final double x2 =
						2 * (endC + 0.5) / rasterImage.getWidth() - 1;
				final double y2 =
						-(2 * (endR + 0.5) / rasterImage.getHeight() - 1);
				rasterImage = lineRasterizer.rasterize(
						rasterImage.cleared(new Color(0,0,0)),
						x1, y1, x2, y2,
						new Color(1f, 1, 1)
				);
				panel.repaint();
			}
		});

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	public void clear() {
		rasterImage = rasterImage.cleared(new Color(0x2f, 0x2f, 0x2f));
	}

	public void present(final Graphics graphics) {
		presenter.present(rasterImage, graphics);
	}

	public void draw() {
		clear();
		rasterImage = rasterImage.withPixel(10,10,
				new Color(0xffff00));
		/*
		rasterImage = lineRasterizer.rasterize(
				rasterImage,
				-0.8, -0.5, 0.8, 0.5,
				new Color(1.0f, 0, 1)
		);
		rasterImage = lineRasterizer.rasterize(
				rasterImage,
				-0.8, 0.5, 0.8, -0.5,
				new Color(1.0f, 0, 1)
		);
		rasterImage = lineRasterizer.rasterize(
				rasterImage,
				-0.8, -0.8, 0.8, -0.8,
				new Color(1.0f, 0, 1)
		);
		rasterImage = new SeedFill4<Color>().fill(rasterImage,
				rasterImage.getWidth() / 2, rasterImage.getHeight() / 2 + 5,
				new Color(0x00ffff),
				pixel -> pixel.equals(new Color(0x2f, 0x2f, 0x2f)));
		*/

		rasterImage =
			new RenderWireframe<Color>(lineRasterizer)
				.render(rasterImage, new Cube(),
					new Camera()
						.withPosition(new Vec3D(5,3,2))
						.withAzimuth(Math.PI)
						.withZenith(-Math.atan(2.0 / 5.0))
						.getViewMatrix()
						.mul(new Mat4PerspRH(
								Math.PI / 3,
								rasterImage.getHeight() / (double) rasterImage.getWidth(),
								0.1, 1000
								)),
					new Color(1.0f, 1, 0));
	}

	public void start() {
		draw();
		panel.repaint();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Canvas(800, 600)::start);
	}

}