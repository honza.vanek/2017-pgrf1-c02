package rasterizationops;

import io.vavr.collection.Stream;
import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;

public class LineRasterizerNaive<PixelType> implements LineRasterizer<PixelType> {
    @NotNull
    @Override
    public RasterImage<PixelType> rasterize(
            @NotNull RasterImage<PixelType> background,
            double x1, double y1, double x2, double y2,
            @NotNull PixelType value) {
        final double ix1 = (x1 + 1) * background.getWidth() / 2;
        final double iy1 = (-y1 + 1) * background.getHeight() / 2;
        final double ix2 = (x2 + 1) * background.getWidth() / 2;
        final double iy2 = (-y2 + 1) * background.getHeight() / 2;
        final double k = (iy2 - iy1) / (ix2 - ix1);
        final double q = iy1 - k * ix1;
        final int c1 = (int) ix1;
        final int c2 = (int) ix2;
        return Stream.rangeClosed(c1, c2).foldLeft(background,
                (currentImage, c) -> {
                    final double ix = c;
                    final double iy = k * ix + q;
                    final int r = (int) iy;
                    return currentImage.withPixel(c, r, value);
                }
        );
    }
}
